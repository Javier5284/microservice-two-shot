# Wardrobify

Team: Javier Hernandez and Nick Chesley.

* Javier - Shoes.
* Nick - Hats.

## Design

## Shoes microservice

Created a Shoe model with the following properties: name, manufacturer, color, picture, and bin(via foreignKey). Also created a binVO model to interact with the wardrobe microservice through a poller to integrate the shoe object.

## Hats microservice

designed a model for a hat object with these properties: fabric, style, color, pictureUrl, and location(via foreignKey). also designed a locationVO model to interact with the wardrobe microservice through a poller to integrate the hat object.
