from django.views.decorators.http import require_http_methods
from .models import Shoe, BinVO
import json
from common.json import ModelEncoder
from django.http import JsonResponse


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href",
        "closet_name",
    ]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "name",
        "id",
    ]
    encoders = {
        "bin": BinVOEncoder(),
    }


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "name",
        "color",
        "picture",
        "bin",
    ]
    encoders = {
        "bin": BinVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeDetailEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            bin = BinVO.objects.get(import_href=content["bin"])
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoes(request, pk):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse(
                {"error": "Shoe not found"},
                status=404,
            )
    elif request.method == "DELETE":
        try:
            count, _ = Shoe.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Shoe.DoesNotExist:
            return JsonResponse(
                {"error": "Shoe not found"},
                status=404,
            )
    else:
        content = json.loads(request.body)
        try:
            shoe = Shoe.objects.get(id=pk)
            if "location" in content:
                location = BinVO.objects.get(id=content["location"])
                content["location"] = location
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        Shoe.objects.filter(id=pk).update(**content)

        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
