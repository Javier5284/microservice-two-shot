import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatForm from './HatForm';
import HatsList from './HatsList';

import ShoesList from './ShoesList';
import ShoesForm from './ShoesForm';
function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="hats">
            <Route path="" element={<HatsList />} />
            <Route path="new" element={<HatForm />} />
          </Route>
          <Route path="shoes" element={<ShoesList />} />
            <Route path="shoes/create" element={<ShoesForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
