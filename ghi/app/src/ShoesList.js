import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";


const Shoeslist = () => {
  const [shoes, setShoes] = useState([]);

  const fetchShoes = async () => {
    const url = "http://localhost:8080/api/shoes/";
    const response = await fetch(url);
    console.log(response);
    if (response.ok) {
      const data = await response.json();
      console.log(data);
      setShoes(data.shoes);
    }
  };

  useEffect(() => {
    fetchShoes();
  }, []);

  const handleDelete = async (shoeId) => {
    const deleteUrl = `http://localhost:8080/api/shoes/${shoeId}/`;
    try {
      const response = await fetch(deleteUrl, {
        method: "DELETE",
      });
      if (response.ok) {
        setShoes(shoes.filter((shoe) => shoe.id !== shoeId));
    }
    else {
      console.log("You aint go no shoe game");
    }
  } catch (err) {
    console.log(err);
  }
};


  return (
    <>
      <div className="container">
        <table className="table table-success table-striped table-hover">
          <thead>
            <tr>
              <th>Manufacturer</th>
              <th>Name</th>
              <th>Color</th>
              <th>Image</th>
              <th>Closet</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {shoes.map((shoe) => (
              <tr key={shoe.id}>
                <td>{shoe.manufacturer}</td>
                <td>{shoe.name}</td>
                <td>{shoe.color}</td>
                <td>
                  <img src={shoe.picture} width ="300" alt="" className="photo"/>
                </td>
                <td>{shoe.bin.closet_name}</td>
                <td>
                  <button onClick={() => handleDelete(shoe.id)} className="btn btn-danger">
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>

    <div className="container">
        <Link to ="/shoes/create" className="btn btn-primary">Create a new shoe
        </Link>
    </div>
    </>
  );
};

export default Shoeslist;
