import React, { useEffect, useState } from "react";

const ShoesForm = () => {
  const [bin, setBinNumber] = useState([]);

  const [formData, setFormData] = useState({
    manufacturer: "",
    name: "",
    color: "",
    picture: "",
  });

  const fetchBin = async () => {
    const url = "http://localhost:8100/api/bins/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setBinNumber(data.bins);
      console.log(data.bins);
    }
  };

  useEffect(() => {
    fetchBin();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const shoesUrl = "http://localhost:8080/api/shoes/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(shoesUrl, fetchConfig);
    if (response.ok) {
      setFormData({
        manufacturer: "",
        name: "",
        color: "",
        picture: "",
      });
    }
  };

  const handleChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="form-group">
        <label htmlFor="manufacturer">Manufacturer</label>
        <input
          value={formData.manufacturer}
          onChange={handleChange}
          placeholder="Enter manufacturer"
          required
          type="text"
          name="manufacturer"
          className="form-control"
          id="manufacturer"
        />
      </div>
      <div className="form-group">
        <label htmlFor="name">Model Name</label>
        <input
          value={formData.name}
          onChange={handleChange}
          required
          placeholder="Enter model name"
          type="text"
          name="name"
          className="form-control"
          id="name"
        />
      </div>
      <div className="form-group">
        <label htmlFor="color">Color</label>
        <input
          value={formData.color}
          required
          onChange={handleChange}
          name="color"
          placeholder="Enter color"
          type="text"
          className="form-control"
          id="color"
        />
      </div>
      <div className="form-group">
        <label htmlFor="picture">Picture URL</label>
        <input
          value={formData.picture}
          onChange={handleChange}
          name="picture"
          type="url"
          className="form-control"
          id="picture"
        />
      </div>
      <div className="mb-3">
        <label htmlFor="bin" className="form-label">
          Bin
        </label>
        <select
          value={formData.bin}
          onChange={handleChange}
          required
          name="bin"
          id="bin"
          className="form-select"
        >
          <option value="">Select a bin</option>
          {bin.map((bin) => (
            <option key={bin.id} value={bin.href}>
              {bin.closet_name} - {bin.bin_number}
            </option>
          ))}
        </select>
      </div>
      <button type="submit" className="btn btn-primary">
        Submit
      </button>
    </form>
  );
};

export default ShoesForm;
