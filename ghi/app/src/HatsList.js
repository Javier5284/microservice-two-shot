import { useEffect, useState } from "react";

function HatsList()  {
    const [hats, setHats] = useState([]);
    const fetchData = async () => {
        const url = "http://localhost:8090/api/hats";
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                const requests = [];
                for (let hat of data.hats) {
                    const details = `http://localhost:8090/api/hats/${hat.id}`;
                    requests.push(fetch(details));
                }
                const hatResponses = await Promise.all(requests);
                const hatData = await Promise.all(hatResponses.map((res) => res.json()));
                setHats(hatData)
            }
        }
        catch (e) {
            console.error(e);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const handleDelete = async (hatId) => {
        const deleteUrl = `http://localhost:8090/api/hats/${hatId}`;
        try {
            const response = await fetch(deleteUrl, {
                method: "DELETE",
            });
            if (response.ok) {
                setHats(hats.filter((hat) => hat.id !== hatId));
            }
            else {
                console.error("I suck and couldn't even delete a hat");
            }
        }
        catch (e) {
            console.error(e);
        }
    };

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>
                        <a href= "http://localhost:3000/hats/new/" role="button" className="btn btn-success">+ Create Hat</a>
                    </th>
                </tr>
                <tr>
                    <th>Fabric</th>
                    <th>Style</th>
                    <th>color</th>
                    <th>image</th>
                    <th>closet</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {hats.map(hat => {
                    return (
                        <tr key={hat.id}>
                            <td>{hat.fabric}</td>
                            <td>{hat.style}</td>
                            <td>{hat.color}</td>
                            <td><img alt="" className="photo" src={hat.picture_URL} width="200"/></td>
                            <td>{hat.location.closet_name} </td>
                            <td>
                                <button onClick={() => handleDelete(hat.id)} className="btn btn-danger">
                                    Delete
                                </button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default HatsList;
